#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto --block-size=M'
alias Rscripts='cd /usr/local/bin/;ls -al'
alias htop='htop -C --no-color'
PS1='[\u@\h \W]\$ '
